const express = require('express')
const cors = require('cors')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const mongoose = require('mongoose')
const indexRouter = require('./routes/index')
const usersRouter = require('./routes/users')
const productsRouter = require('./routes/product')
const authRouter = require('./routes/auth')
const dotenv = require('dotenv')
const { authenMiddleware, authorizeMiddleware } = require('./helper/auth')
const { ROLE } = require('./constant')
const eventsRouter = require('./routes/events')

// // get config vars
dotenv.config()

mongoose.connect('mongodb://localhost:27017/example')

const app = express()
app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
app.use('/users', authenMiddleware, authorizeMiddleware([ROLE.ADMIN, ROLE.LOCAL_ADMIN]), usersRouter)
app.use('/product', authenMiddleware, authorizeMiddleware([ROLE.ADMIN, ROLE.LOCAL_ADMIN, ROLE.USER]), productsRouter)
app.use('/auth', authRouter)
app.use('/events', eventsRouter)

module.exports = app
