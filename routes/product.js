const express = require('express')

const Product = require('../models/Product')
const router = express.Router()

const getProducts = async function (req, res, next) {
  try {
    const products = await Product.find({}).exec()
    res.status(200).json(products)
  } catch (err) {
    return res.status(500).send({
      mseeage: err.mseeage
    })
  }
}
const addProducts = async function (req, res, next) {
  const newProduct = new Product({
    name: req.body.name,
    price: parseFloat(req.body.price)
  })
  try {
    await newProduct.save()
    res.status(201).json(newProduct)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getProduct = async function (req, res, next) {
  const id = req.params.id
  try {
    const product = await Product.findById(id).exec()
    if (product === null) {
      return res.status(404).json({
        message: 'Product not found'
      })
    }
    res.json(product)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}
const updateProduct = async function (req, res, next) {
  const productId = req.params.id

  try {
    const productUp = await Product.findById(productId)
    productUp.name = req.body.name
    productUp.price = parseFloat(req.body.price)
    await productUp.save()
    return res.status(200).json(productUp)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}
const deleteProduct = async function (req, res, next) {
  const productId = req.params.id

  try {
    await Product.findByIdAndDelete(productId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getProducts)
router.get('/:id', getProduct)
router.post('/', addProducts)
router.put('/:id', updateProduct)
router.delete('/:id', deleteProduct)

module.exports = router
