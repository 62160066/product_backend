const mongoose = require('mongoose')
const Event = require('../models/Events')
mongoose.connect('mongodb://localhost:27017/example')
async function clearEvent () {
  await Event.deleteMany({})
}

async function main () {
  await clearEvent()
  await Event.insertMany([
    {
      title: 'title 1',
      content: 'Content 1',
      startDate: new Date('2022-03-29 08:00'),
      endDate: new Date('2022-03-29 17:00'),
      class: 'a'
    },
    {
      title: 'title 2',
      content: 'Content 2',
      startDate: new Date('2022-03-30 08:00'),
      endDate: new Date('2022-03-30 17:00'),
      class: 'a'
    },
    {
      title: 'title 3',
      content: 'Content 3',
      startDate: new Date('2022-03-24 08:00'),
      endDate: new Date('2022-03-24 17:00'),
      class: 'a'
    },
    {
      title: 'title 4',
      content: 'Content 4',
      startDate: new Date('2022-03-21 08:00'),
      endDate: new Date('2022-03-21 11:00'),
      class: 'a'
    },
    {
      title: 'title 5',
      content: 'Content 5',
      startDate: new Date('2022-03-21 12:00'),
      endDate: new Date('2022-03-21 14:00'),
      class: 'a'
    }
  ])
}
main().then(function () {
  console.log('Finish')
})
